/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import java.math.BigDecimal;
import java.util.Date;

import org.codehaus.jettison.json.JSONException;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.test.utils.DistributionOrderHeaderData;
import org.openbravo.distributionorder.test.utils.DistributionOrderLineData;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.distributionorder.test.utils.IssueOrReceiptDOHeaderData;
import org.openbravo.distributionorder.test.utils.IssueOrReceiptDOLineData;
import org.openbravo.distributionorder.test.utils.ShipmentInOutHeaderData;
import org.openbravo.distributionorder.test.utils.ShipmentInOutLineData;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOBasicTests extends OBDO_FB_WeldBaseTest {

  @Before
  public void initialize() {
    log.info("Initializing Distribution Orders Basic Tests...");
    super.initialize();
  }

  /**
   * This test checks that it is possible to correctly create and complete a basic Distribution
   * Order Receipt. It also checks that it is possible to close the document without any Issues
   * <ul>
   * <li>1. Create a Distribution Order Receipt</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Close the Distribution Order Receipt and verify that the status is correclty
   * updated</li>
   * </ul>
   */

  @Test
  public void DOB010_CreateCompleteCloseDOR() {
    OBContext.setAdminMode();
    try {
      Product product = DistributionOrderTestUtils.cloneProduct(ALE_BEER_PRODUCT_ID, "DOB010");
      createStockForForTestDOB010(product);
      log.info("**product name: " + product.getName());
      DistributionOrder distributionOrderReceipt = createDOReceiptForDOB010(product);
      verifyDOIssueCorrectlyCreatedForDOB010(distributionOrderReceipt);
      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderReceipt);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void createStockForForTestDOB010(Product product) {
    ShipmentInOut goodsReceipt = getShipmentInoutHeaderForDOB010();
    createAndInsertGoodsReceiptLinesForDOB010(goodsReceipt, product, new BigDecimal(1000000));
    DistributionOrderTestUtils.processShipmentInOut(goodsReceipt);
  }

  private ShipmentInOut getShipmentInoutHeaderForDOB010() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForShipmentInOut("DOB010");
    ShipmentInOutHeaderData shipmentInOutHeaderData = new ShipmentInOutHeaderData.ShipmentInOutHeaderDataBuilder() //
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .documentNo(docNo)
        .documentTypeID(DistributionOrderTestUtils.MM_RECEIPT_US_DOCTYPE_ID)//
        .warehouseID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .businessPartnerID(BE_SOFT_BPARTNER_ID)//
        .build();
    return DistributionOrderTestUtils.createAndSaveGoodsReceiptHeader(shipmentInOutHeaderData);
  }

  private void createAndInsertGoodsReceiptLinesForDOB010(ShipmentInOut goodsReceipt,
      Product product, BigDecimal movementQty) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForGoodsReceipt(goodsReceipt);
    ShipmentInOutLineData shipmentInoutLineData = new ShipmentInOutLineData.ShipmentInOutLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .movementQty(movementQty)//
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .storageBinID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .build();
    DistributionOrderTestUtils.createAndInsertGoodsReceiptLine(shipmentInoutLineData, goodsReceipt);
  }

  private DistributionOrder createDOReceiptForDOB010(Product product) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderForDOB010();
    createAndInsertDistributionOrderLinesForDOB010(distributionOrder, product);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderForDOB010() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOB010");
    DocumentType doReceiptDocType = DistributionOrderTestUtils.getDistributionOrderReceiptDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(false)//
        .docTypeID(doReceiptDocType.getId())//
        .docNo(docNo)//
        .description("DOB010 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesForDOB010(DistributionOrder distributionOrder,
      Product product) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .product(product.getId())//
        .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .lineNo(lineNo)//
        .qtyOrdered(new BigDecimal(10))//
        .build();
    DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
        distributionOrder);
  }

  private void verifyDOIssueCorrectlyCreatedForDOB010(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderIssue = distributionOrder.getReferencedDistorder();
    OBDal.getInstance().refresh(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderIssue, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasRequestedStatus(distributionOrderIssue);
    DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderIssue, 1);
  }

  /**
   * This test checks that it is possible to correctly create, complete and finally close a
   * Distribution Order Issue
   * <ul>
   * <li>1. Clone a Product to create a new one</li>
   * <li>2. Create and complete a Distribution Order Issue for that Product</li>
   * <li>3. Check that the related Distribution Order Receipt has been created correctly</li>
   * <li>4. Close the Distribution Order Issue and verify that it has been done correctly</li>
   * </ul>
   */
  @Test
  public void DOB020_CompleteAndCloseDOIssue() {
    OBContext.setAdminMode();
    try {
      Product product = DistributionOrderTestUtils.cloneProduct(ALE_BEER_PRODUCT_ID, "DOB020");

      DistributionOrder distributionOrderIssue = createDOIssueForDOB020(product);
      verifyDOReceiptCorrectlyCreatedForDOB020(distributionOrderIssue);
      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderIssue);
      DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
          0L);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private DistributionOrder createDOIssueForDOB020(Product product) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderForDOB020();
    createAndInsertDistributionOrderLinesForDOB020(distributionOrder, product);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderForDOB020() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOB020");
    DocumentType doIssueDocType = DistributionOrderTestUtils.getDistributionOrderIssueDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(true)//
        .docTypeID(doIssueDocType.getId())//
        .docNo(docNo)//
        .description("DOB020 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesForDOB020(DistributionOrder distributionOrder,
      Product product) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .product(product.getId())//
        .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .lineNo(lineNo)//
        .qtyConfirmed(new BigDecimal(10))//
        .build();
    DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
        distributionOrder);
  }

  private void verifyDOReceiptCorrectlyCreatedForDOB020(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderReceipt = distributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderReceipt);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderReceipt);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderReceipt, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasConfirmedStatus(distributionOrderReceipt);
    DistributionOrderTestUtils.assertThatDistributionOrderHasReceiptStatus(distributionOrderReceipt,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderReceipt, 1);
  }

  /**
   * This test will check the full Distribution Orders flow without any particular conditions
   * <ul>
   * <li>1. Create a Distribution Order Receipt</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>3. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * </ul>
   */

  @Test
  public void DOB030_FullBasicFlowDistributionOrders() {
    OBContext.setAdminMode();
    try {
      Product product = DistributionOrderTestUtils.cloneProduct(ALE_BEER_PRODUCT_ID, "DOB030");
      createStockForForTestDOB030(product);

      DistributionOrder distributionOrderReceipt = createDOReceiptForDOB030(product);
      verifyDOIssueCorrectlyCreatedForDOB030(distributionOrderReceipt);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      updateConfirmedQtyForDOB030(distributionOrderIssue);

      InternalMovement issueDistributionOrder = createIssueDOForDOB030(distributionOrderIssue,
          product, new BigDecimal(10), new BigDecimal(10), 100L);
      createReceiveDOForDOB030(issueDistributionOrder, new BigDecimal(10), 100L);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void createStockForForTestDOB030(Product product) {
    ShipmentInOut goodsReceipt = getShipmentInoutHeaderForDOB030();
    createAndInsertGoodsReceiptLinesForDOB030(goodsReceipt, product, new BigDecimal(1000000));
    DistributionOrderTestUtils.processShipmentInOut(goodsReceipt);
  }

  private ShipmentInOut getShipmentInoutHeaderForDOB030() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForShipmentInOut("DOB030");
    ShipmentInOutHeaderData shipmentInOutHeaderData = new ShipmentInOutHeaderData.ShipmentInOutHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .documentNo(docNo)//
        .documentTypeID(DistributionOrderTestUtils.MM_RECEIPT_US_DOCTYPE_ID)//
        .warehouseID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .businessPartnerID(BE_SOFT_BPARTNER_ID)//
        .build();
    return DistributionOrderTestUtils.createAndSaveGoodsReceiptHeader(shipmentInOutHeaderData);
  }

  private void createAndInsertGoodsReceiptLinesForDOB030(ShipmentInOut goodsReceipt,
      Product product, BigDecimal movementQty) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForGoodsReceipt(goodsReceipt);
    ShipmentInOutLineData shipmentInoutLineData = new ShipmentInOutLineData.ShipmentInOutLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .movementQty(movementQty)//
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .storageBinID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .build();
    DistributionOrderTestUtils.createAndInsertGoodsReceiptLine(shipmentInoutLineData, goodsReceipt);
  }

  private DistributionOrder createDOReceiptForDOB030(Product product) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderForDOB030();
    createAndInsertDistributionOrderLinesForDOB030(distributionOrder, product);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderForDOB030() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOB030");
    DocumentType doReceiptDocType = DistributionOrderTestUtils.getDistributionOrderReceiptDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(false)//
        .docTypeID(doReceiptDocType.getId())//
        .docNo(docNo)//
        .description("DOB030 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesForDOB030(DistributionOrder distributionOrder,
      Product product) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .product(product.getId())//
        .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .lineNo(lineNo)//
        .qtyOrdered(new BigDecimal(10))//
        .build();
    DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
        distributionOrder);
  }

  private void verifyDOIssueCorrectlyCreatedForDOB030(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderIssue = distributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderIssue);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderIssue, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasRequestedStatus(distributionOrderIssue);
    DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderIssue, 1);
  }

  private void updateConfirmedQtyForDOB030(DistributionOrder issueDistributionOrder) {
    DistributionOrderLine issueDOLine = issueDistributionOrder.getOBDODistributionOrderLineList()
        .get(0);
    DistributionOrderTestUtils.updateConfirmedQtyAndVerifyRelatedDOIsUpdated(issueDOLine,
        new BigDecimal(10));
  }

  private InternalMovement createIssueDOForDOB030(DistributionOrder distributionOrderIssue,
      Product product, BigDecimal issuedQty, BigDecimal totalIssuedQty, long issuedPercentage)
      throws JSONException {
    InternalMovement issueDistributionOrder = getIssueDistributionOrderHeaderForDOB030(
        distributionOrderIssue);
    createAndInsertIssueDOLinesForDOB030(issueDistributionOrder,
        distributionOrderIssue.getOBDODistributionOrderLineList().get(0), product, issuedQty);
    DistributionOrderTestUtils.processIssueDOAndVerifyIsProcessed(issueDistributionOrder);
    DistributionOrderTestUtils.assertThatIssueDoLineHasUpdatedRelatedDocs(
        issueDistributionOrder.getMaterialMgmtInternalMovementLineList().get(0), totalIssuedQty,
        issuedPercentage);
    return issueDistributionOrder;
  }

  private InternalMovement getIssueDistributionOrderHeaderForDOB030(
      DistributionOrder distributionOrderIssue) {
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOB030");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .name(docNo)//
        .description("DOB030")//
        .movementDate(new Date())//
        .distributionOrderID(distributionOrderIssue.getId())//
        .inTransitBinID(DistributionOrderTestUtils.EC_200_LOCATOR_ID)//
        .isSalesTransaction(true)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

  private void createAndInsertIssueDOLinesForDOB030(InternalMovement issueDistributionOrder,
      DistributionOrderLine distributionOrderLine, Product product, BigDecimal issuedQty) {
    long lineNo = DistributionOrderTestUtils
        .getNextLineNoForIssueOrReceiptDO(issueDistributionOrder);
    IssueOrReceiptDOLineData issueOrReceiptDOLineData = new IssueOrReceiptDOLineData.IssueOrReceiptDOLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_EAST_ORG_ID)//
        .lineNo(lineNo)//
        .productID(product.getId())//
        .uomID(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .movementQty(issuedQty)//
        .storageBinFromID(DistributionOrderTestUtils.EC_000_LOCATOR_ID)//
        .storageBinToID(DistributionOrderTestUtils.EC_200_LOCATOR_ID)//
        .distributionOrderLineID(distributionOrderLine.getId())//
        .build();
    DistributionOrderTestUtils.createAndInsertIssueOrReceiptDOLine(issueOrReceiptDOLineData,
        issueDistributionOrder);
  }

  private InternalMovement createReceiveDOForDOB030(InternalMovement issueDistributionOrder,
      BigDecimal totalReceivedQty, long receiveStatusPercentage) throws JSONException {
    InternalMovement receiveDistributionOrder = getReceiveDistributionOrderHeaderForDOB030(
        issueDistributionOrder);
    DistributionOrderTestUtils.processReceiveDOAndVerifyIsProcessed(receiveDistributionOrder,
        DistributionOrderTestUtils.WC_000_LOCATOR_ID);
    DistributionOrderTestUtils.assertThatReceiveDoLineHasUpdatedRelatedDocs(
        receiveDistributionOrder.getMaterialMgmtInternalMovementLineList().get(0), totalReceivedQty,
        receiveStatusPercentage);
    return issueDistributionOrder;
  }

  private InternalMovement getReceiveDistributionOrderHeaderForDOB030(
      InternalMovement issueDistributionOrder) {
    String issueDOViewID = DistributionOrderTestUtils
        .getIDOfViewRecordForIssueDO(issueDistributionOrder);
    String docNo = DistributionOrderTestUtils.getNextDocNoForIssueOrReceiptDO("DOB030");
    IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData = new IssueOrReceiptDOHeaderData.IssueOrReceiptDOHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_WEST_ORG_ID)//
        .name(docNo)//
        .description("DOB030")//
        .movementDate(new Date())//
        .isSalesTransaction(false)//
        .issueDOViewID(issueDOViewID)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveIssueOrReceiveDOHeader(issueOrReceiptDOHeaderData);
  }

}
